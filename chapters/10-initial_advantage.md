# The Initial Advantages of the Urban Guerrilla

The dynamics of urban guerrilla warfare lie in the guerrilla's violent clash with the military and police forces of the dictatorship. In this conflict, the police have superiority. The urban guerrilla has inferior forces. The paradox is that the urban guerrilla is nevertheless the attacker.

The military and police forces, for their part, respond to the conflict by mobilizing and concentrating greatly superior forces in the pursuit and destruction of the urban guerrilla. The guerrilla can only avoid defeat if he depends on the initial advantages he has and knows how to exploit them to the end, to compensate for his weakness and lack of material.

The initial advantages are:

1. He must take the enemy by surprise.
2. He must know the terrain of the encounter.
3. He must have greater mobility and speed than the police and other repressive forces.
4. His information service must be better than the enemy's.
5. He must be in command of the situation, and demonstrate a decisiveness so great that everyone on our side is inspired and never thinks of hesitating, while on the other side the enemy is stunned and incapable of acting.
