# Kidnapping

Kidnapping is capturing and holding in a secret place a spy, political personality or a notorious and dangerous enemy of the revolutionary movement. Kidnapping is used to exchange or liberate imprisoned revolutionaries or to force the suspension of torture in jail by the military dictatorship.

The kidnapping of personalities who are well-known artists, sports figures or who are outstanding in some other field, but who have evidenced no political interest, can be a useful form of propaganda for the guerrillas, provided it occurs under special circumstances, and is handled so the public understands and sympathizes with it. The kidnappings of foreigners or visitors constitutes a form of protest against the penetration and domination of imperialism in our country.
