# The Shot; the Urban Guerrilla's Reason for Existence

The urban guerrilla's reason for existence, the basic condition in which he acts and survives, is to shoot. The urban guerrilla must know how to shoot well, because it is required by this type of combat.

In conventional warfare, combat is generally at a distance with long-range weapons. In unconventional warfare, in which urban guerrilla warfare is included, combat is at short range and often very close. To prevent his own death, the urban guerrilla must shoot first, and he cannot err in his shot. He cannot waste his ammunition because he does not possess large amounts, and so he must conserve it. Nor can he replace his ammunition quickly, since he is a part of a small team in which each guerrilla has to be able to look after himself. The urban guerrilla can lose no time, and thus has to be able to shoot at once.

One basic fact, which we want to emphasize completely, and whose importance cannot be overestimated, is that the urban guerrilla must not fire continuously, using up his ammunition. It may be that the enemy is responding to this fire precisely because he is waiting until the guerrilla's ammunition is all used up. At such a moment, without having the opportunity to replace his ammunition, the guerrilla faces a rain of enemy fire, and can be taken prisoner or killed.

In spite of the value of the surprise factor, which many times makes it unnecessary for the urban guerrilla to use his weapons, he cannot be allowed the luxury of entering combat without knowing how to shoot. And when face-to-face with the enemy, he must always be moving from one position to another, since to stay in one place makes him a fixed target and, as such, very vulnerable.

The urban guerrilla's life depends on shooting, on his ability to handle his weapons well and to avoid being hit. When we speak of shooting, we speak of accuracy as well. Shooting must be practiced until it becomes a reflex action on the part of the urban guerrilla. To learn how to shoot and have good aim, the urban guerrilla must train himself systematically, utilizing every practice method shooting at targets, even in amusement parks and at home.

Shooting and marksmanship are the urban guerrilla's water and air. His perfection of the art of shooting may make him a special type of urban guerrilla—that is, a sniper, a category of solitary combatant indispensible in isolated actions. The sniper knows how to shoot at close range and at long range, and his weapons are appropriate for either type of shooting.
