# Assaults

Assaults are the armed attacks which we make to expropriate funds, liberate prisoners, capture explosives, submachine guns, and other types of weapons and ammunition. Assaults can take place in broad daylight or at night. Daytime assaults are made when the objective cannot be achieved at any other hour, such as the transport of money by banks, which is not done at night. Night assault is usually the most advantageous for the guerrilla. The ideal is for all assaults to take place at night, when conditions for a surprise attack are most favorable and the darkness facilitates escape and hides the identity of the participants. The urban guerrilla must prepare himself, nevertheless, to act under all conditions, daytime as well as night.

The must vulnerable targets for assaults are the following:

1. Credit establishments
2. Commercial and industrial enterprises, including plants for the manufacture of weapons and explosives
3. Military establishments
4. Commissaries and police stations
5. Jails
6. Government property
7. Mass communications media
8. North American firms and properties
9. Government vehicles, including military and police vehicles, trucks, armored vehicles, money carriers, trains, ships, and airplanes.

The assaults on businesses use the same tactics, because in every case the buildings represent a fixed target. Assaults on buildings are planned as guerrilla operations, varied according to whether they are against banks, a commercial enterprise, industries, military bases, commissaries, prisons, radio stations, warehouses for foreign firms, etc.

The assault on vehicles—money-carriers, armored vehicles, trains, ships, airplanes—are of another nature, since they are moving targets. The nature of the operation varies according to the situation and the circumstances—that is, whether the vehicle is stationary or moving. Armored cars, including military vehicles, are not immune to mines. Roadblocks, traps, ruses, interception by other vehicles, Molotov cocktails, shooting with heavy weapons, are efficient methods of assaulting vehicles. Heavy vehicles, grounded airplaces and anchored ships can be seized and their crews and guards overcome. Airplanes in flight can be hijacked by guerrilla action or by one person. Ships and trains in motion can be assaulted or captured by guerrilla operations in order to obtain weapons and ammunition or to prevent troop movements.
