# The Firing Group

In order to function, the urban guerrillas must be organized into small groups. A team of no more than four or five is called a firing group. A minimum of two firing groups, separated and insulated from other firing groups, directed and coordinated by one or two persons, this is what makes a firing team.

Within the firing group, there must be complete confidence among the members. The best shot, and the one who knows best how to handle the submachine gun, is the person in charge of operations.

The firing group plans and executes urban guerrilla actions, obtains and stores weapons, and studies and corrects its own tactics.

When there are tasks planned by the strategic command, these tasks take preference. But there is no such thing as a firing group without its own initiative. For this reason, it is essential to avoid any rigidity in the guerrilla organization, in order to permit the greatest possible initiative on the part of the flrlng group. The old-type hierarchy, the style of the traditional revolutionaries, doesn't exist in our organization. This means that, except for the priority of the objectives set by the strategic command, any firing group can decide to raid a bank, to kidnap or execute an agent of the dictatorship, a figure identified with the reaction, or a foreign spy, and can carry out any type of propaganda or war of nerves against the enemy, without the need to consult with the general command.

No firing group can remain inactive waiting for orders from above. Its obligation is to act. Any single urban guerrilla who wants to establish a firing group and begin action can do so, and thus becomes a part of the organization.

This method of action eliminates the need for knowing who is carrying out which actions, since there is free initiative and the only important point is to greatly increase the volume of urban guerrilla activity in order to wear out the government and force it onto the defensive.

The firing group is the instrument of organized action. Within it, guerrilla operations and tactics are planned, launched and carried through to success. The general command counts on the firing groups to carry out objectives of a strategic nature, and to do so in any part of the country. For its part, the general command helps the firing groups with their difficulties and with carrying out objectives of a strategic nature, and to do so in any part of the country.

The organization is an indestructable network of firing groups, and of coordinations among them, that functions simply and practically within a general command that also participates in attacks—an organization that exists for no other purpose than that of pure and simple revolutionary action.
