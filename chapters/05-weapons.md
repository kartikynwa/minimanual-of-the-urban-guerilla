# The Urban Guerrilla's Weapons

The urban guerrilla's weapons are light arms, easily obtained, usually captured from the enemy, purchased, or made on the spot. Light weapons have the advantage of fast handling and easy transport. In general, light weapons are characterized as being short-barrelled. This includes many automatic weapons. Automatic and semi-automatic weapons considerably increase the firepower of the urban guerrilla. The disadvantage of this type of weapon, for us, is the difficulty in controlling it, resulting in wasted rounds or a wasteful use of ammunition—corrected for only by a good aim and precision firing. Men who are poorly trained convert automatic weapons into an ammunition drain.

Experience has shown that the basic weapon of the urban guerrilla is the light submachine gun. This weapon, in addition to being efficient and easy to shoot in an urban area, has the advantage of being greatly respected by the enemy. The guerrilla must thoroughly know how to handle the submachine gun, now so popular and indispensible to the Brazilian urban guerrillas.

The ideal submachine gun for the urban guerrilla is the INA .45 caliber. Other types of submachine guns of different calibers can also be used—understanding of course, the problem of ammunition. Thus, it is preferable that the manufacturing capabilities of the urban guerrillas be used for the production of one type of submachine gun, so that the ammunition to be used can be standardized. Each firing group of urban guerrillas must have a submachine gun handled by a good marksman. The other members of the group must be armed with .38 revolvers, our standard weapon. The .32 is also useful for those who want to participate. But the .38 is preferable since its impact usually puts the enemy out of action.

Hand grenades and conventional smoke bombs can also be considered light weapons, with defensive power for cover and withdrawal.

Long-barrelled weapons are more difficult for the urban guerrilla to transport, and they attract much attention because of their size. Among the long-barrelled weapons are the FAL, the Mauser guns or rifles, hunting guns such as the Winchester, and others.

Shotguns can be useful if used at close range and point blank. They are useful even for a poor shot, especially at night when precision isn't much help. A pressure airgun can be useful for training in marksmanship. Bazookas and mortars can also be used in action, but the conditions for using them have to be prepared and the people who use them must be trained.

The urban guerrilla should not attempt to base his actions on the use of heavy weapons, which have major drawbacks in a type of fighting that demands lightweight weapons to insure mobility and speed.

Homemade weapons are often as efficient as the best weapons produced in conventional factories, and even a sawed-off shotgun is a good weapon for the urban guerrilla fighter.

The urban guerrilla's role as a gunsmith has a basic importance. As a gunsmith, he takes care of the weapons, knows how to repair them, and in many cases can set up a small shop for improvising and producing effective small arms.

Experience in metallurgy and on the mechanical lathe are basic skills the urban guerrilla should incorporate into his manufacturing plans for the construction of homemade weapons. This production, and courses in explosives and sabotage, must be organized. The primary materials for practice in these courses must be obtained ahead of time, to prevent an incomplete apprenticeship—that is to say, so as to leave no room for experimentation.

Molotov cocktails, gasoline, homemade contrivances such as catapaults and mortars for firing explosives, grenades made of pipes and cans, smoke bombs, mines, conventional explosives such as dynamite and potassium chlorate, plastic explosives, gelatine capsules, and ammunition of every kind are indispensible to the success of the urban guerrilla's mission.

The methods of obtaining the necessary materials and munitions will be to buy them or to take them by force in expropriation actions specially planned and carried out. The urban guerrillas will be careful not to keep explosives and other materials that can cause accidents around for very long, but will always try to use them immediately on their intended targets.

The urban guerrilla's weapons and his ability to maintain them constitute his firepower. By taking advantage of modern weapons and introducing innovations in his firepower and in the use of certain weapons, the urban guerrilla can improve many of the tactics of urban warfare. An example of this was the innovation made by the Brazilian urban guerrillas when they introduced the use of the submachine gun in their attacks on banks.

When the massive use of uniform submachine guns becomes possible, there will be new changes in urban guerrilla warfare tactics. The firing group that utilizes uniform weapons and corresponding ammunition, with reasonable care for their maintenance, will reach a considerable level of effectiveness.

The urban guerrilla increases his effectiveness as he increases his firepower.
