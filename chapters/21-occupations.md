# Occupations 

Occupations are a type of attack carried out when the urban guerrilla stations himself in specific establishments and locations, for a temporary action against the enemy or for some propaganda purpose. The occupation of factories and schools during strikes, or at other times, is a method of protest or of distracting the enemy's attention. The occupation of radio stations is for propaganda purposes.

Occupation is a highly effective model for action but, in order to prevent losses and material damage to our forces, it is always a good idea to plan on the possibility of a forced withdrawal. It must always be meticulously planned, and carried out at the opportune moment. Occupations always have a time limit, and the swifter they are completed, the better.
