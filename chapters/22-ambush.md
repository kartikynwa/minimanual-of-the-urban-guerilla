# Ambush

Ambushes are attacks, typified by surprise, when the enemy is trapped on the road or when he makes a police net surrounding a house or estate. A false alarm can bring the enemy to the spot, where he falls into a trap.

The principle object of the ambush is to capture enemy weapons and to punish him with death. Ambushes to halt passenger trains are for propaganda purposes, and, when they are troop trains, the object is to annihilate the enemy and seize his weapons. The urban guerrilla sniper is the kind of fighter specially suited for ambush, because he can hide easily in the irregularities of the terrain, on the roofs and the tops of buildings and apartments under construction. From windows and dark places, he can take careful aim at his chosen target.

Ambush has devestating effects on the enemy, leaving him unnerved, insecure and fearful.
