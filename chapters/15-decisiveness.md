# Decisiveness

It is not enough for the urban guerrilla to have in his favor surprise, speed, knowledge of the terrain, and information. He must also demonstrate his command of any situation and a capacity for decisiveness, without which all other advantages will prove to be useless.

It is impossible to carry out any action, however well-planned, if the urban guerrilla turns out to be indecisive, uncertain, irresolute. Even an action successfully begun can end in defeat if command of the situation and the capacity for decision falter in the middle of the execution of the plan. When this command of the situation and a capacity for decision are absent, the void is filled with hesitation and terror. The enemy takes advantage of this failure and is able to liquidate us.

The secret of the success of any operation, simple or complex, easy or difficult, is to rely on determined men. Strictly speaking, there are no simple operations: all must be carried out with the same care taken in the most difficult, beginning with the choice of the human elements—which means relying on leadership and the capacity for decision in every situation.

One can see ahead of time whether an action will be successfull or not by the way its participants act during the preparatory period. Those who fall behind, who fail to make designated contacts, are easily confused, forget things, fail to complete the basic tasks of the work, possibly are indecisive men and can be a danger. It is better not to include them.

Decisiveness means to put into practice the plan that has been devised with determination, with audacity, and with an absolute firmness. It takes only one person who hesitates to lose all.
