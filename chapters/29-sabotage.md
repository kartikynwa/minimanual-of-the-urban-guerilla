# Sabotage

Sabotage is a highly destructive type of attack using very few persons—and sometimes requiring only one—to accomplish the desired result. When the urban guerrilla uses sabotage, the first step is isolated sabotage. Then comes the step of dispersed and general sabotage, carried out by the population. Well-executed sabotage demands study, planning and careful action. A characteristic form of sabotage is explosion, using dynamite, fire or the placing of mines. A little sand, a trickle of any kind of combustible, a poor lubrication job, a screw removed, a short circuit, inserted pieces of wood or iron, can cause irreparable damage. The objective of sabotage is to hurt, to damage, to make useless and to destroy vital enemy points such as the following:

1. The economy of the country
2. Agricultural or industrial production
3. Transport and communication systems
4. Military and police systems and their establishments and depots
5. The repressive military-police system
6. The firms and properties of exploiters in the country

The urban guerrilla should endanger the economy of the country, particularly its economic and financial aspects, such as its domestic and foreign banking network, its exchange and credit systems, its tax collection system, etc.

Public offices, centers of government and government depots are easy targets for sabotage. Nor will it be easy to prevent the sabotage of agricultural and industrial production by the urban guerrilla, with his thorough knowledge of the local situation. Factory workers acting as urban guerrillas are excellent industrial saboteurs, since they, better than anyone, understand the industry, the factory, the machinery or the part most likely to destroy an entire operation, doing much more damage than a poorly-informed layman could do.

With respect to the enemy's transport and communications systems, beginning with railway traffic, it is necessary to attack them systematically with sabotage. The only caution is against causing death and injury to passengers, especially regular commuters on suburban and long-distance trains. Attacks on freight trains, rolling or stationary stock, stoppage of military transports and communciations systems, these are the major objectives in this area. Sleepers can be damaged and pulled up, as can rails. A tunnel blocked by a barrier of explosives, or an obstruction caused by a derailed car, causes enormous harm.

The derailment of a train carrying fuel is of major damage to the enemy. So is dynamiting a railroad bridge. In a system where the size and weight of the rolling equipment is enormous, it takes months for workers to repair or rebuild the destruction and damage. As for highways, they can be obstructed with trees, stationary vehicles, ditches, dislocation of barriers by dynamite, and bridges destroyed by explosions. Ships can be damaged at anchor in seaports or riverports, or in the shipyards. Aircraft can be destroyed or damaged on the ground. Telephone and telegraph lines can be systematically damaged, their towers blown up, and their lines made useless. Transport and communications must be sabotaged immediately because the revolutionary movement has already begun in Brazil, and it is essential to impede the enemy's movement of troops and munitions.

Oil lines, fuel plants, depots for bombs and ammunition arsenals, military camps and bases must become targets for sabotage operations, while vehicles, army trucks and other military or police vehicles must be destroyed wherever they are found. The military and police repression centers and their specialized organs must also claim the attention of the guerrilla saboteur. Foreign firms and properties in the country, for their part, must become such frequent targets of sabotage that the volume of actions directed against them surpasses the total of all other actions against enemy vital points.
