# Personal Qualities of the Urban Guerrilla

The urban guerrilla is characterized by his bravery and his decisive nature. He must be a good tactician, and a good marksman. The urban guerrilla must be a person of great cleverness to compensate for the fact that he is not sufficiently strong in weapons, ammunition and equipment.

The career military officers and the government police have modern weapons and transport, and can go about anywhere freely, using the force of their own strength. The urban guerrilla does not have such resources at his disposal, and leads a clandestine existence. The guerrilla may be a convicted person or one who is out on parole, and must then use false documents.

Nevertheless, the urban guerrilla has an advantage over the conventional military or the police. It is that, while the military and the police act on behalf of the enemy, whom the people hate, the urban guerrilla defends a just cause, which is the people's cause.

The urban guerrilla's weapons are inferior to the enemy's, but from the moral point of view, the urban guerrilla has an undeniable superiority. This moral superiority is what sustains the urban guerrilla. Thanks to it, the urban guerrilla can accomplish his principle duty, which is to attack and survive.

The urban guerrilla has to capture or steal weapons from the enemy to be able to fight. Because his weapons are not uniform—since what he has are expropriated or have fallen into his hands in various ways—the urban guerrilla faces the problem of a variety of weapons and a shortage of ammunition. Moreover, he has no place in which to practice shooting and marksmanship. These difficulties have to be overcome, forcing the urban guerrillas to be imaginative and creative—qualities without which it would be impossible for him to carry out his role as a revolutionary.

The urban guerrilla must possess initiative, mobility and flexibility, as well as versatility and a command of any situation. Initiative especially is an indispensible quality. It is not always possible to foresee everything, and the urban guerrilla cannot let himself become confused, or wait for instructions. His duty is to act, to find adequate solutions for each problem he faces, and to retreat. It is better to err acting than to do nothing for fear of making a mistake. Without initiative, there is no urban guerrilla warfare.

Other important qualities in the urban guerrilla are the following: to be a good walker, to be able to stand up against fatigue, hunger, rain or heat. To know how to hide, and how to be vigilant. To conquer the art of dissembling. Never to fear danger. To behave the same by day as by night. Not to act impetuously. To have unlimited patience. To remain calm and cool in the worst of conditions and situations. Never to leave a track or trail. Not to get discouraged.

In the face of the almost insurmountable difficulties in urban guerrilla warfare, sometimes comrades weaken and give up the fight.

The urban guerrilla is not a businessman in an urban company, nor is he an actor in a play. Urban guerrilla warfare, like rural guerrilla warfare, is a pledge which the guerrilla makes to himself. When he can no longer face the difficulties, or if he knows that he lacks the patience to wait, then it is better for him to relinquish his role before he betrays his pledge, for he clearly lacks the basic qualities necessary to be a guerrilla.
