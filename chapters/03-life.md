# How the Urban Guerrilla Lives

The urban guerrilla must know how to live among the people, and he must be careful not to appear strange and different from ordinary city life. He should not wear clothes that are different from those that other people wear. Elaborate and high-fashion clothing for men or women may often be a handicap if the urban guerrilla's mission takes him into working class neighborhoods, or sections where such dress is uncommon. The same care has to be taken if the urban guerrilla must move from the South of the country to the North, and vice versa.

The urban guerrilla must make his living through his job or his professional activity. If he is known and sought by the police, he must go underground, and sometimes must live hidden. Under such circumstances, the urban guerrilla cannot reveal his activity to anyone, since this information is always and only the responsibility of the revolutionary organization in which he is participating.

The urban guerrilla must have a great ability for observation. He must be well-informed about everything, particularly about the enemy's movements, and he must be very inquisitive and knowledgable about the area in which he lives, operates, or travels through.

But the fundamental characteristic of the urban guerrilla is that he is a man who fights with weapons; given these circumstances, there is very little likelihood that he will be able to follow his normal profession for long without being identified by the police. The role of expropriation thus looms as clear as high noon. It is impossible for the urban guerrilla to exist and survive without fighting to expropriate.

Thus, the armed struggle of the urban guerrilla points towards two essential objectives:

1. The physical elimination of the leaders and assistants of the armed forces and of the police;
2. The expropriation of government resources and the wealth belonging to the rich businessmen, the large landowners and the imperialists, with small expropriations used for the sustenance of the individual guerrillas and large ones for the maintenance of the revolutionary organization itself.

It is clear that the armed struggle of the urban guerrilla also has other objectives. But here we are referring to the two basic objectives, above all expropration. It is necessary for every urban guerrilla to always keep in mind that he can only maintain his existence if he is able to kill the police and those dedicated to repression, and if he is determined—truly determined—to expropriate the wealth of the rich businessmen, landowners and imperialists.

One of the fundamental characteristics of the Brazilian revolution is that, from the beginning, it developed around the expropriation of the wealth of the major business, imperialist and landowning interests, without excluding the largest and most powerful commercial elements engaged in the import-export business. And by expropriating the wealth of the principle enemies of the people, the Brazilian revolution was able to hit them at their vital center, with preferential and systematic attacks on the banking network—that is to say, the most telling blows were levelled at the businessman's nerve system.

The bank robberies carried out by the Brazilian urban guerrillas hurt big businesses and others, the foreign companies which insure and re-insure the banking capital, the imperialist companies, the federal and state governments—all of them are systematically expropriated as of now.

The fruit of these expropriations has been devoted to the tasks of learning and perfecting urban guerrilla techniques, the purchase, production and transportation of weapons and ammunition for the rural areas, the security precautions of the guerrillas, the daily maintenance of the fighters, those who have been liberated from prison by armed force, those who have been wounded, and those who are being persecuted by the police, and to any kind of problem concerning comrades liberated from jail or assassinated by the police and the military dictatorship.

The tremendous costs of the revolutionary war must fall upon the big businesses, on the imperialists, on the large landowners, and on the government too—both federal and state—since they are all exploiters and oppressors of the people. Men of the government, agents of the dictatorship and of foreign imperialism, especially, must pay with their lives for the crimes they have committed against the Brazilian people.

In Brazil, the number of violent actions carried out by urban guerrillas, including executions, explosions, seizures of weapons, ammunition and explosives, assaults on banks and prisons, etc., is significant enough to leave no room for doubt as to the actual aims of the revolutionaries; all are witnesses to the fact that we are in a full revolutionary war and that this war can be waged only by violent means.

This is the reason why the urban guerrilla uses armed struggle, and why he continues to concentrate his efforts on the physical extermination of the agents of repression, and to dedicate 24 hours a day to expropriations from the people's exploiters.
