# Information

The chances that the government has for discovering and destroying the urban guerrillas lessens as the power of the dictatorship's enemies becomes greater and more concentrated among the population.

This concentration of the opponents of the dictatorship plays a very important role in providing information about the actions of the police and government officials, as well as hiding the activities of the guerrillas. The enemy can also be thrown off with false information, which is worse for him because it is a tremendous waste.

By whatever means, the sources of information at the disposal of the urban guerrilla are potentially better than those of the police. The enemy is observed by the people, but he does not know who among the people transmits information to the urban guerrillas. The military and the police are hated by the people for the injustices and violence they have committed, and this facilitates obtaining information which is damaging to the activities of government agents.

Information, which is only a small segment of popular support, represents an extraordinary potential in the hands of the urban guerrilla.

The creation of an intelligence service, with an organized structure, is a basic need for us. The urban guerrilla has to have vital information about the plans and movements of the enemy; where they are, how they move, the resources of their banking network, their means of communication, and the secret activities they carry out. The reliable information passed on to the guerrillas represents a well-aimed blow at the dictatorship. The dictatorship has no way to defend itself in the face of an important leak which facilitates our destructive attacks.

The enemy also wants to know what actions we are planning so he can destroy us or prevent us from acting. In this sense, the danger of betrayal is present, and the enemy encourages betrayal and infiltrates spies into the guerrilla organization. The urban guerrilla's technique against this enemy tactic is to denounce publicly the spies, traitors, informers and provocateurs. Since our struggle takes place among the people and depends on their sympathy—while the government has a bad reputation because of its brutality, corruption and incompetence—the informers, spies, traitors and the police come to be enemies of the people, without supporters, denounced to the urban guerrillas and, in many cases, properly punished.

For his part, the urban guerrilla must not evade the duty—once he knows who the spy or informer is—of physically wiping him out. This is the proper method, approved by the people, and it minimizes considerably the incidence of infiltration or enemy spying.

For complete success in the battle against spies and informers, it is essential to organize a counter-espionage or counter-intelligence service. Nevertheless, as far as information is concerned, it cannot all be reduced to a matter of knowing the enemy's moves and avoiding the infiltration of spies. Intelligence information must be broad—it must embrace everything, including the most insignificant material. There is a technique of obtaining information, and the urban guerrilla must master it. Following this technique, intelligence information is obtained naturally, as a part of the life of the people.

The urban guerrilla, living in the midst of the population and moving about among them, must be attentive to all types of conversations and human relations, learning how to disguise his interest with great skill and judgement.

In places where people work, study, and live, it is easy to collect all kinds of information on payments, business, plans of all kinds, points of view, opinions, people's state of mind, trips, interior layout of buildings, offices and rooms, operations centers, etc.

Observation, investigation, reconnaissance, and exploration of the terrain are also excellent sources of information. The urban guerrilla never goes anywhere absentmindedly and without revolutionary precaution, always on the alert lest something occurs. Eyes and ears open, senses alert, his memory is engraved with everything necessary, now or in the future, to the continued activity of the guerrilla fighter.

Careful reading of the press with particular attention to the mass communication media, the research of accumulated data, the transmission of news and everything of note, a persistence in being informed and in informing others, all this makes up the intricate and immensely complicated question of information which gives the urban guerrilla a decisive advantage.
