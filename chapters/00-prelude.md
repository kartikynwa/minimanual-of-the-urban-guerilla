# Introduction {-}

I would like to make a two-fold dedication of this work; first, to the memories of Edson Souto, Marco Antonio Bras de Carvalho, Melson Jose de Almeida ("Escoteiro") and so many other heroic fighters and urban guerrillas who fell at the hands of the assassins of the Military Police, the Army, the Navy, the Air Force, and the DOPS, hated instruments of the repressive military dictatorship.

Second, to the brave comrades—men and women—imprisoned in the medieval dungeons of the Brazilian Government and subjected to tortures that even surpass the horrendous crimes carried out by the Nazis. Like those comrades whose memories we revere, as well as those taken prisoner in combat, what we must do is fight.

Each comrade who opposes the military dictatorship and wants to oppose it can do something, however small the task may seem. I urge all who read this minimanual and decide that they cannot remain inactive, to follow its instructions and join the struggle now. I ask this because, under any theory and under any circumstances, the duty of every revolutionary is to make the revolution.

Another important point is not merely to read this minimanual here and now, but to circulate its contents. This circulation will be possible if those who agree with its ideas make mimeographed copies or print it in a booklet, (although in this latter case, armed struggle itself will be necessary.)

Finally, the reason why this minimanual bears my signature is that the ideas expressed or systematized here reflect the personal experiences of a group of people engaged in armed struggle in Brazil, among whom I have the honor to be included. So that certain individuals will have no doubts about what this minimanual says, and can no longer deny the facts or continue to say that the conditions for armed struggle do not exist, it is necessary to assume responsibility for what is said and done. Therefore, anonymity becomes a problem in a work like this. The important fact is that there are patriots prepared to fight like soldiers, and the more there are the better.

The accusation of "violence" or "terrorism" no longer has the negative meaning it used to have. It has aquired new clothing; a new color. It does not divide, it does not discredit; on the contrary, it represents a center of attraction. Today, to be "violent" or a "terrorist" is a quality that ennobles any honorable person, because it is an act worthy of a revolutionary engaged in armed struggle against the shameful military dictatorship and its atrocities.

*— Carlos Marighella, 1969*
