# How to Carry Out the Action

The urban guerrilla who correctly carries through his apprenticeship and training must give the greatest possible importance to his method of carrying out actions, for in this he cannot commit the slightest error. Any carelessness in learning tactics and their use invites certain disaster, as experience teaches us every day. Common criminals commit errors frequently because of their tactics, and this is one of the reasons why the urban guerrillas must be so insistently preoccupied with following revolutionary tactics, and not the tactics of bandits. And not only for that reason. There is no urban guerrilla worthy of the name who ignores the revolutionary method of action and fails to practice it rigorously in the planning and execution of his activities.

"The giant is known by his toe." The same can be said of the urban guerrilla, who is known from afar by his correct tactics and his absolute fidelity to principle.

The revolutionary method of carrying out actions is strongly and forcefully based on the knowledge and use of the following elements;

1. Investigation and intelligence gathering
2. Observation and vigilance
3. Reconnaissance, or exploration of the terrain
4. Study and timing of routes
5. Mapping
6. Mechanization
7. Careful selection of personnel
8. Selection of firepower
9. Study and practice in success
10. Success
11. Use of cover
12. Retreat
13. Dispersal
14. The liberation or transfer of prisoners 15. the elimination of evidence l6. the rescue of wounded
