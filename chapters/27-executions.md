# Executions 

Execution is the killing of a foreign spy, of an agent of the dictatorship, of a police torturer, of a dictatorial personality in the government involved in crimes and persecutions against patriots, of a stool pigeon, informer, police agent or police provocateur. Those who go to the police of their own free will to make denunciations and accusations, who supply information and who finger people, must be executed when they are caught by the urban guerrillas.

Execution is a secret action, in which the least possible number of urban guerrillas are involved. In many cases, the execution can be carried out by a single sniper, patient, alone and unknown, and operating in absolute secrecy and in cold blood.
