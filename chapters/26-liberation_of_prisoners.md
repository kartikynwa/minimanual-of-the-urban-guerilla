# Liberation of Prisoners

The liberation of prisoners is an armed action designed to free jailed urban guerrillas. In daily struggle against the enemy, the urban guerrilla is subject to arrest, and can be sentenced to unlimited years in jail.

This does not mean that the battle ends here. For the guerrilla, his experience is deepened by prison, and struggle continues even in the dungeons where he is held. The imprisoned guerrilla views the prisons of the enemy as a terrain which he must dominate and understand in order to free himself by a guerrilla operation. There is no jail, either on an island, in a city penitentiary, or on a farm, that is impregnable to the slyness, cleverness and firepower of the rebels. The urban guerrilla who is free views the jails of the enemy as the inevitable site of guerrilla actions designed to liberate his ideological comrades from prison. It is this combination of the urban guerrilla in freedom and the urban guerrilla in jail that results in the armed operations we refer to as "liberation of prisoners".

The guerrilla operations that can be used in liberating prisoners are the following;

1. Riots in penal establishments, in correctional colonies or camps, or on transport or prison ships;
2. Assaults on urban or rural prisons, detention centers, prison camps, or any other permanent or temporary place where prisoners are held;
3. Assaults on prisoner transport trains or convoys;
4. Raids and penetrations of prisons;
5. Ambushing of guards who move prisoners.
