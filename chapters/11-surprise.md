# Surprise

To compensate for his general weakness and shortage of weapons compared to the enemy, the urban guerrilla uses surprise. The enemy has no way to combat surprise and becomes confused and is destroyed.

When urban guerrilla warfare broke out in Brazil, experience proved that surprise was essential to the success of any guerrilla operation. The technique of surprise is based upon four essential requirements:

1. We know the situation of the enemy we are going to attack, usually by means of precise information and meticulous observation, while the enemy does not know he is going to be attacked and knows nothing about the attackers.
2. We know the strength of the enemy we are going to attack, and the enemy knows nothing about our strength.
3. Attacking by surprise, we save and conserve our forces, while the enemy is unable to do the same, and is left at the mercy of events.
4. We determine the time and place of the attack, fix its duration and establish its objectives. The enemy remains ignorant of all of this information.
