# The Logistics of the Urban Guerrilla

Conventional logistics can be expressed with the formula FFEA: F—food F—fuel E—equipment A—ammunition

Conventional logistics refer to the maintenance problems for an army or a regular armed force, transported in vehicles, with fixed bases and supply lines. Urban guerrillas, on the contrary, are not an army but small armed groups, intentionally fragmented. They have neither vehicles nor rear areas. Their supply lines are precarious and insufficient, and they have no fixed bases except in the rudimentary sense of a weapons factory within a house. While the goal of conventional logistics is to supply the war needs of the "gorillas" who are used to repress rural and urban rebellion, urban guerrilla logistics aim at sustaining operations and tactics which have nothing in common with conventional warfare and are directed against the government and foreign domination of the country.

For the urban guerrilla, who starts from nothing and who has no support at the beginning, logistics are expressed by the formula MMWAE, which is:

M—mechanization M—money W—weapons A—ammunition E—explosives

Revolutionary logistics takes mechanization as one of its bases. Nevertheless, mechanization is inseperable from the driver. The urban guerrilla driver is as important as the urban guerrilla machine gunner. Without either, the machines do not work, and the automobile, as well as the submachine gun becomes a dead thing. An experienced driver is not made in one day, and apprenticeship must begin early. Every good urban guerrilla must be a driver. As to the vehicles, the urban guerrilla must expropriate what he needs. When he already has resources, the urban guerrilla can combine the expropriation of vehicles with his other methods of acquisition.

Money, weapons, ammunition and explosives, and automobiles as well, must be expropriated. The urban guerrilla must rob banks and armories, and seize explosives and ammunition wherever he finds them.

None of these operations is carried out for just one purpose. Even when the raid is to obtain money, the weapons that the guards carry must be taken as well.

Expropriation is the first step in organizing our logistics, which itself assumes an armed and permanently mobile character.

The second step is to reinforce and expand logistics, resorting to ambushes and traps in which the enemy is surprised and his weapons, ammunition, vehicles and other resources are captured.

Once he has weapons, ammunition and explosives, one of the most serious logistics problems facing the urban guerrilla is a hiding place in which to leave the material, and appropriate means of transporting it and assembling it where it is needed. This has to be accomplished even when the enemy is alerted and has the roads blocked.

The knowledge that the urban guerrilla possesses of the terrain, and the devices he uses or is capable of using, such as scouts specially prepared and recruited for this mission, are the basic elements in solving the eternal logistics problems faced by the guerrillas.
