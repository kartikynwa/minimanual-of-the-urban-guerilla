# The Bank Assault as Popular Mission

The most popular mission is the bank assault. In Brazil, the urban guerrillas have begun a type of organized assault on the banks as a guerrilla operation. Today, this type of assault is widely used, and has served as a sort of preliminary test for the urban guerrilla in his training in the tactics of urban guerrilla warfare.

Important innovations in the tactics of assaulting banks have developed, guaranteeing escape, the withdrawal of money, and the anonymity of those involved. Among these innovations, we cite the shooting of tires of cars to prevent pursuit, locking people in the bank bathroom, making them sit on the floor, immobilizing the bank guards and taking their weapons, forcing someone to open the safe or the strong box, and using disguises.

Attempts to install bank alarms, to use guards or electronic detection devices prove fruitless when the assault is political and is carried out according to urban guerrilla warfare techniques. This guerrilla method uses new techniques to meet the enemy's tactical changes, has access to firepower that is growing every day, becomes increasingly more experienced and more confident, and uses a larger number of guerrillas every time; all to guarantee the success of operations planned down to the last detail.

The bank assault is a typical expropriation. But, as is true with any kind of armed expropriatory action, the guerrilla is handicapped by a two-fold competition:

1. Competition from the outlaw
2. Competition from the right-wing counter-revolutionary

This competition produces confusion, which is reflected in the people's uncertainty. It is up to the urban guerrilla to prevent this from happening, and to accomplish this he must use two methods:

1. He must avoid the outlaw's technique, which is one of unnecessary violence and the expropriation of goods and possessions belonging to the people.
2. He must use the assault for propaganda purposes at the very moment it is taking place, and later distribute material, leaflets—every possible means of explaining the objectives and the principles of the urban guerrillas, as expropriator of the government and the ruling elite.
