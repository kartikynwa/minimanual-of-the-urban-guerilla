# On the Types and Nature of Missions for the Urban Guerrilla

In order to achieve the objectives previously listed, the urban guerrilla is obliged, in his tactics, to follow missions whose nature is as different or diversified as possible. The urban guerrilla does not arbitrarily choose this or that mission. Some actions are simple; others are complicated. The inexperienced guerrilla must be gradually introduced into actions and operations which run from the simple to the complex. He begins with small missions and tasks until he becomes completely experienced.

Before any action, the urban guerrilla must think of the methods and the personnel at his disposal to carry out the mission. Operations and actions that demand the urban guerrilla's technical preparation cannot be carried out by someone who lacks the technical skill. With these precautions, the missions which the urban guerrilla can undertake are the following:

1. Assaults
2. Raids and penetrations
3. Occupations
4. Ambushes
5. Street tactics
6. Strikes and work stoppages
7. Desertions, diversions, seizures, expropriation of weapons, ammunition and explosives
8. Liberation of prisoners
9. Executions
10. Kidnappings
11. Sabotage
12. Terrorism
13. Armed propaganda
14. War of nerves
