# Characteristics of the Urban Guerrilla's Tactics

 The tactics of the urban guerrilla have the following characteristics:

1. It is an aggressive tactic, or, in other words, it has an offensive character. As is well known, defensive action means death for us. Since we are inferior to the enemy in firepower, and have neither his resources nor his power base, we cannot defend ourselves against an offensive or a concentrated attack by the "gorillas". That is the reason why our urban technique can never be permanent, can never defend a fixed base nor remain in any one spot waiting to repell the circle of repression.
2. It is a tactic of attack and rapid withdrawal, by which we preserve our forces.
3. It is a tactic that aims at the development of urban guerrilla warfare, whose function will be to wear out, demoralize and distract the enemy forces, permitting the emergence and survival of rural guerrilla warfare, which is destined to play the decisive role in the revolutionary war.
