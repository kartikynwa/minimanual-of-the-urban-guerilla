# A Definition of the Urban Guerrilla

 The urban guerrilla is a person who fights the military dictatorship with weapons, using unconventional methods. A revolutionary and an ardent patriot, he is a fighter for his country's liberation, a friend of the people and of freedom. The area in which the urban guerrilla operates is in the large Brazilian cities. There are also criminals or outlaws who work in the big cities. Many times, actions by criminals are taken to be actions by urban guerrillas.

The urban guerrilla, however, differs radically from the criminal. The criminal benefits personally from his actions, and attacks indiscrimminately without distinguishing between the exploiters and the exploited, which is why there are so many ordinary people among his victims. The urban guerrilla follows a political goal, and only attacks the government, the big businesses and the foreign imperialists.

Another element just as harmful to the guerrillas as the criminal, and also operating in the urban area, is the counterrevolutionary, who creates confusion, robs banks, throws bombs, kidnaps, assassinates, and commits the worst crimes imaginable against urban guerrillas, revolutionary priests, students, and citizens who oppose tyranny and seek liberty.

The urban guerrilla is an implacable enemy of the regime, and systematically inflicts damage on the authorities and on the people who dominate the country and exercise power. The primary task of the urban guerrilla is to distract, to wear down, to demoralize the military regime and its repressive forces, and also to attack and destroy the wealth and property of the foreign managers and the Brazilian upper class.

The urban guerrilla is not afraid to dismantle and destroy the present Brazilian economic, political and social system, for his aim is to aid the rural guerrillas and to help in the creation of a totally new and revolutionary social and political structure, with the armed population in power.
