# Mobility and Speed

To insure a mobility and speed that the police cannot match, the urban guerrilla needs the following:

1. Mechanization
2. Knowledge of the terrain
3. A disruption or suspension of enemy transport and communications
4. Light weapons

By carefully carrying out operations that last only a few moments, and leaving the site in mechanized vehicles, the urban guerrilla beats a rapid retreat, escaping capture.

The urban guerrilla must know the way in detail, and, in this manner, must go through the schedule ahead of time as a training, to avoid entering alleyways that have no exit, or running into traffic jams, or being stopped by the Transit Department's traffic signals.

The police pursue the urban guerrilla blindly, without knowing which road he is using for his escape. While the urban guerrilla escapes quickly because he knows the terrain, the police lose the trail and give up the chase.

The urban guerrilla must launch his operations far from the logistical centers of the police. A primary advantage of this method of operation is that it places us at a reasonable distance from the possibility of capture, which facilitates our evasion.

In addition to this necessary precaution, the urban guerrilla must be concerned with the enemy's communication system. The telephone is the primary target in preventing the enemy from access to information, by knocking out his communications systems.

Even if he knows about the guerrilla operation, the enemy depends on modern transportation for his logistics support, and his vehicles necessarily lose time carrying him through the heavy traffic of the large cities. It is clear that the tangled and treacherous traffic is a disadvantage for the enemy, as it would be for us if we were not ahead of him.

If we want to have a safe margin of security and be certain to leave no tracks for the future, we can adopt the following methods:

1. Deliberately intercept the police with other vehicles, or by seemingly casual inconveniences and accidents; but in this case the vehicles in question should neither be legal nor have real license numbers
2. Obstruct the roads with fallen trees, rocks, ditches, false traffic signs, dead ends or detours, or other clever methods
3. Place homemade mines in the way of the police; use gasoline or throw Molotov cocktails to set their vehicles on fire
4. Set off a burst of submachine gun fire or weapons such as the FAL aimed at the motor and tires of the cars engaged in the pursuit

With the arrogance typical of the police and the military authorities, the enemy will come to fight us equipped with heavy guns and equipment, and with elaborate maneuvers by men armed to the teeth. The urban guerrilla must respond to this with light weapons that can be easily transported, so he can always escape with maximum speed without ever accepting open fighting. The urban guerrilla has no mission other than to attack and quickly withdraw. We would leave ourselves open to the most crushing defeats if we burdened ourselves with heavy weapons and with the tremendous weight of the ammunition necessary to use them, at the same time losing our precious gift of mobility.

When our enemy fights against us with the cavalry, we are at no disadvantage as long as we are mechanized. The automobile goes faster than the horse. From within the car, we also have the target of the mounted police, knocking him down with submachine gun and revolver fire or with Molotov cocktails and hand grenades.

On the other hand, it is not so difficult for an urban guerrilla on foot to make a target of a policeman on horseback. Moreover, ropes across the street, marbles, and cork stoppers are very efficient methods of making them both fall. The great disadvantage faced by the mounted policeman is that he presents the urban guerrilla with two excellent targets—the horse and its rider.

Apart from being faster than the horseman, the helicopter has no better chance in pursuit. If the horse is too slow compared to the urban guerrilla's automobile, the helicopter is too fast. Moving at 200 kilometers an hour, it will never succeed in hitting from above a target that is lost among the crowds and street vehicles, nor can the helicopter land in public streets in order to capture someone. At the same time, whenever it flies too low, it will be excessively vulnerable to the fire of the urban guerrillas.
