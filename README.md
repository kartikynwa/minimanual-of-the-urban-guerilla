# Minimanual of the Urban Guerilla

By Carlos Marighella.

Based on [ebook-boilerplate](https://github.com/phodal/ebook-boilerplate).

Text from [Marxists.org](https://www.marxists.org/archive/marighella-carlos/1969/06/minimanual-urban-guerrilla/index.htm).

## Requirements

To build this book, you will need:

- `pandoc`.
- TexLive for PDF output.
- `kindlegen` for MOBI output.

## Instructions

Clone the repo and do:

`make {all,epub,pdf,mobi,rtf}`
