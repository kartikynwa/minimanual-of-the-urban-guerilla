#!/bin/sh

read -p "book name：" bookname
read -p "author：" author

sed -i "s/Phodal Huang/$author/g" "build/author.html" "build/metadata.xml" "build/title.txt" "Makefile"
sed -i "s/电子书模板/$bookname/g" "build/author.html" "build/metadata.xml" "build/title.txt" "Makefile"
